SERVER = server
CLIENT = client
CC = gcc
FLAGS = -Wall -Werror -Wextra
LIB = -L./libft -lft
LIBFT = libft/libft.a
HEADER = includes
OBJS = objects/
SERVER_FILE = server.c
CLIENT_FILE = client.c
SERVER_OBJ = $(addprefix $(OBJS), $(SERVER_FILE:.c=.o))
CLIENT_OBJ = $(addprefix $(OBJS), $(CLIENT_FILE:.c=.o))

all: $(OBJS) $(SERVER) $(CLIENT)

$(OBJS):
	mkdir -p $(OBJS)

$(SERVER): $(SERVER_OBJ) 
	make -C libft
	$(CC) $(FLAGS) -o $(SERVER) $(SERVER_OBJ) $(LIBFT)

$(CLIENT): $(CLIENT_OBJ)
	$(CC) $(FLAGS) -o $(CLIENT) $(CLIENT_OBJ) $(LIBFT)

$(OBJS)%.o : %.c
	$(CC) $(FLAGS) -o $@ -c $< -I$(HEADER)

clean:
	make clean -C libft
	rm -rf $(OBJS)

fclean: clean
	make fclean -C libft
	rm -f $(SERVER) $(CLIENT)

re: fclean all

.PHONY: all fclean clean re
