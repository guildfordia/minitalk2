/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/31 11:55:34 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/31 12:40:49 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strnlen(const char *str, size_t max_len)
{
	size_t		len;

	len = 0;
	while (len < max_len)
	{
		if (!str)
			break ;
		len++;
		str++;
	}
	return (len);
}
