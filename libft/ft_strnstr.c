/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:52:39 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/02 22:32:56 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t big_len)
{
	size_t		little_len;
	const char	*start;

	little_len = ft_strlen(little);
	if (little_len == 0)
		return ((char *)big);
	start = big;
	while (*start && start + little_len <= big + big_len)
	{
		if (*start == *little && ft_strncmp(start + 1, little + 1,
				little_len - 1) == 0)
			return ((char *)start);
		start++;
	}
	return (0);
}
