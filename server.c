/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/14 20:09:02 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/14 20:09:06 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minitalk.h"

static void	handle_sigusr(int sig)
{
	static char	c = 0;
	static int	bit = 0;

	if (sig == SIGUSR1)
		c |= (1 << (bit));
	else if (sig == SIGUSR2)
		c &= ~(1 << (bit));
	bit++;
	if (bit == 7)
	{
		ft_putchar_fd(c, 1);
		bit = 0;
		c = 0x00;
	}
}

int	main(void)
{
	struct sigaction	act;

	act.sa_handler = &handle_sigusr;
	act.sa_flags = SA_RESTART;
	ft_putnbr_fd(getpid(), 1);
	write(1, "\n", 1);
	while (1)
	{
		if (sigaction(SIGUSR1, &act, NULL) != 0)
			exit(1);
		if (sigaction(SIGUSR2, &act, NULL) != 0)
			exit(1);
	}
	return (0);
}
